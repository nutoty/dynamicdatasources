package com.wycc.dynamic.aspect;

import com.wycc.dynamic.config.DynamicDataSourceProvider;
import com.wycc.dynamic.utils.DynamicDataSourceContextHolder;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;
import java.security.cert.Extension;

/**
 * @Author yong.b.wu
 * @Date: 2022/05/13
 */
@Aspect
@Component
@Order(10)
public class GlobalDataSourceAspect {

    @Autowired
    HttpSession session;

    @Pointcut("execution(* com.wycc.dynamic.service.*.*(..))")
    public void ps(){

    }

    @Around("ps()")
    public Object around(ProceedingJoinPoint pjp){
        DynamicDataSourceContextHolder.setDataSourceType((String) session.getAttribute(DynamicDataSourceProvider.DATASOURCE_TYPE_SESSION));
        try {
            return pjp.proceed();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            DynamicDataSourceContextHolder.clearDataSourceType();
        }
        return null;
    }

}
