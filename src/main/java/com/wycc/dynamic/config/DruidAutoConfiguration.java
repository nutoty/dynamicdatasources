package com.wycc.dynamic.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author yong.b.wu
 * @Date: 2022/05/10
 */
@Configuration
public class DruidAutoConfiguration {
    @Autowired
    DynamicDataSourceProvider dynamicDataSourceProvider;

    @Bean
    DynamicDataSource dynamicDataSource(){
        return new DynamicDataSource(dynamicDataSourceProvider);
    }


}
