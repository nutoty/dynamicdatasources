package com.wycc.dynamic.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

/**
 * @Author yong.b.wu
 * @Date: 2022/05/10
 */
@ConfigurationProperties(prefix = "spring.datasource")
public class DruidProperties {
    private int initialSize;
    private int minIdle;
    private int maxActive;
    private int maxWait;
    private int timeBetweenEvictionRunsMillis;
    private int minEvictableIdleTimeMillis;
    private int maxEvictableIdleTimeMillis;
    private String validationQuery;
    private boolean testWhileIdle;
    private boolean testOnBorrow;
    private boolean testOnReturn;
    private Map<String, Map<String, String>> ds;

    public DruidDataSource dataSource(DruidDataSource dataSource){
         /** 配置初始化大小 最大 最小 */
        dataSource.setInitialSize(initialSize);
        dataSource.setMaxActive(maxActive);
        dataSource.setMinIdle(minIdle);
        /** 配置获取连接等待超时的时间 */
        dataSource.setMaxWait(maxWait);
        /**配置间隔多久才进行一次检测,检测需要关闭的空闲连接,单位毫秒*/
        dataSource.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
        /**配置一个连接在池中最大 最小生存时间,单位毫秒*/
        dataSource.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
        dataSource.setMaxEvictableIdleTimeMillis(maxEvictableIdleTimeMillis);
        /**
         * 用来检测连接是否有效的sql, 要求是一个查询语句,常用select 'x',
         * 如果 validationQuery 为null, testWhileIdle,testOnBorrow,testOnReturn 都不会起作用
         */
        dataSource.setValidationQuery(validationQuery);
        /**
         * 建议配置为true,不影响性能,并且保证安全性,申请连接的时候检测,
         * 如果空闲时间大于timeBetweenEvictionRunsMillis,执行validationQuery检测连接是否有效
         */
        dataSource.setTestWhileIdle(testWhileIdle);
        /**申请连接时执行validationQuery,检测连接是否有效,做了这个配置会降低性能*/
        dataSource.setTestOnBorrow(testOnBorrow);
        /**申请连接时执行validationQuery,检测连接是否有效,做了这个配置会降低性能*/
        dataSource.setTestOnBorrow(testOnReturn);
        return dataSource;
    }

    public int getInitialSize() {
        return this.initialSize;
    }

    public void setInitialSize(int initialSize) {
        this.initialSize = initialSize;
    }

    public int getMinIdle() {
        return this.minIdle;
    }

    public void setMinIdle(int minIdle) {
        this.minIdle = minIdle;
    }

    public int getMaxActive() {
        return this.maxActive;
    }

    public void setMaxActive(int maxActive) {
        this.maxActive = maxActive;
    }

    public int getMaxWait() {
        return this.maxWait;
    }

    public void setMaxWait(int maxWait) {
        this.maxWait = maxWait;
    }

    public int getTimeBetweenEvictionRunsMillis() {
        return this.timeBetweenEvictionRunsMillis;
    }

    public void setTimeBetweenEvictionRunsMillis(int timeBetweenEvictionRunsMillis) {
        this.timeBetweenEvictionRunsMillis = timeBetweenEvictionRunsMillis;
    }

    public int getMinEvictableIdleTimeMillis() {
        return this.minEvictableIdleTimeMillis;
    }

    public void setMinEvictableIdleTimeMillis(int minEvictableIdleTimeMillis) {
        this.minEvictableIdleTimeMillis = minEvictableIdleTimeMillis;
    }

    public int getMaxEvictableIdleTimeMillis() {
        return this.maxEvictableIdleTimeMillis;
    }

    public void setMaxEvictableIdleTimeMillis(int maxEvictableIdleTimeMillis) {
        this.maxEvictableIdleTimeMillis = maxEvictableIdleTimeMillis;
    }

    public String getValidationQuery() {
        return this.validationQuery;
    }

    public void setValidationQuery(String validationQuery) {
        this.validationQuery = validationQuery;
    }

    public boolean getTestWhileIdle() {
        return this.testWhileIdle;
    }

    public void setTestWhileIdle(boolean testWhileIdle) {
        this.testWhileIdle = testWhileIdle;
    }

    public boolean getTestOnBorrow() {
        return this.testOnBorrow;
    }

    public void setTestOnBorrow(boolean testOnBorrow) {
        this.testOnBorrow = testOnBorrow;
    }

    public boolean getTestOnReturn() {
        return this.testOnReturn;
    }

    public void setTestOnReturn(boolean testOnReturn) {
        this.testOnReturn = testOnReturn;
    }

    public Map<String, Map<String, String>> getDs() {
        return this.ds;
    }

    public void setDs(Map<String, Map<String, String>> ds) {
        this.ds = ds;
    }
}
