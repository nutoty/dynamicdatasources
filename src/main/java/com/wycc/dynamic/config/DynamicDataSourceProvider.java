package com.wycc.dynamic.config;

import javax.sql.DataSource;
import java.util.Map;

public interface DynamicDataSourceProvider {
    String DEFAULT_DATASOURCE = "master";
    String DATASOURCE_TYPE_SESSION = "dataTypeSession";

    /**
     * 加载所有的数据源
     * @return
     */
    Map<String, DataSource> loadDataSources();
}
