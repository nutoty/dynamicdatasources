package com.wycc.dynamic.controller;

import com.wycc.dynamic.config.DynamicDataSourceProvider;
import com.wycc.dynamic.entity.Pillar;
import com.wycc.dynamic.service.PillarService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @Author yong.b.wu
 * @Date: 2022/05/13
 */
@RestController
public class DataSourceController {

    private static final Logger log = LoggerFactory.getLogger(DataSourceController.class);

    @Autowired
    private PillarService pillarService;

    @PostMapping("/ds")
    public void changDataSource(String dsType, HttpSession session){
        session.setAttribute(DynamicDataSourceProvider.DATASOURCE_TYPE_SESSION, dsType);
        log.info("切换数据源为：{}",dsType);
    }

    @GetMapping("/pillar")
    public List<Pillar> getPillar(){
        List<Pillar> pillars = pillarService.findAll();
        return pillars;
    }

}
