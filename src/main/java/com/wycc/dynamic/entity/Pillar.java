package com.wycc.dynamic.entity;

/**
 * @Author yong.b.wu
 * @Date: 2022/05/13
 */
public class Pillar {
    private String code;
    private String name;

    public Pillar() {
    }

    public Pillar(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Pillar{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
