package com.wycc.dynamic.mapper;

import com.wycc.dynamic.entity.Pillar;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Author yong.b.wu
 * @Date: 2022/05/10
 */
public interface PillarMapper {
    @Select("select count(*) from pillar")
    Integer count();

    List<Pillar> getPillar();
}
