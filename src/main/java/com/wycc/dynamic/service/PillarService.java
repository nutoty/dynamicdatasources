package com.wycc.dynamic.service;

import com.wycc.dynamic.annotation.DataSource;
import com.wycc.dynamic.entity.Pillar;
import com.wycc.dynamic.mapper.PillarMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author yong.b.wu
 * @Date: 2022/05/10
 */
@Service
public class PillarService {
    @Autowired
    PillarMapper pillarMapper;

    @DataSource("master")
    public Integer master(){
        return pillarMapper.count();
    }

    @DataSource("slave")
    public Integer slave(){
        return pillarMapper.count();
    }

    @DataSource("slave")
    public List<Pillar> findAll(){
        return pillarMapper.getPillar();
    }

}
