package com.wycc.dynamic;

import com.wycc.dynamic.entity.Pillar;
import com.wycc.dynamic.service.PillarService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class DynamicApplicationTests {
    @Autowired
    PillarService pillarService;

    @Test
    void contextLoads() {
        System.out.println("systemLogService.master() = " + pillarService.master());
        System.out.println("systemLogService.slave() = " + pillarService.slave());
    }

    @Test
    public void testPillar(){
        List<Pillar> pillars = pillarService.findAll();
        System.out.println(pillars);
    }

}
