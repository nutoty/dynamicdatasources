/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 80026
Source Host           : localhost:3306
Source Database       : portalcoredb

Target Server Type    : MYSQL
Target Server Version : 80026
File Encoding         : 65001

Date: 2022-05-13 16:33:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pillar
-- ----------------------------
DROP TABLE IF EXISTS `pillar`;
CREATE TABLE `pillar` (
  `code` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
